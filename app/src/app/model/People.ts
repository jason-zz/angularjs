export class People {
  id: string;
  firstName: string;
  lastName: string;
  picture: string;
  bio: string;

  static create(data) {
    return new People(data);
  }

  constructor(data) {
    this.id         = data.id;
    this.firstName  = data.firstName;
    this.lastName   = data.lastName;
    this.picture    = data.picture;
    this.bio        = data.bio;
  }
}