import { Http, Response } from '@angular/http';
import { Component } from '@angular/core';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { DataService } from './services/DataService';
import { People } from './model/People';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public savedPeople: any;
  public title = 'Profile Browser';
  public currentProfileContent: any;
  public currentImg: string;
  public currentFirstName: string;
  public currentLastName: string;
  
  constructor(private dataService: DataService) {}

  ngOnInit() {
    this.getAllPeople();
  }

  getAllPeople() {
    this.dataService.getPeople()
                       .subscribe(
                            data => { this.savedPeople = this.sortPeople(data); },
                            err => {
                                // Log errors if any
                                console.log(err);
                          });
  }
  
  sortPeople(data) {
    data.sort((a, b) => { 
      if (a.firstName < b.firstName) return -1;
      else if (a.firstName > b.firstName) return 1;
      else return 0;
    });
    return data;
  }
  
  updateProfileContent(person, event:any) {
    this.currentProfileContent = this.contentStripHTML(person.bio);
    this.currentImg = person.picture;
    this.currentFirstName = person.firstName;
    this.currentLastName = person.lastName;
    
    if(this.currentProfileContent) {
      this.currentProfileContent = this.currentProfileContent.replace( /\n/g, "|NL|" ).split( "|NL|" )
    }
    
    let l = event.target.parentElement.getElementsByClassName('active');
    let count = l.length;
    for(let i=0;i<count;i++){
      l[i].className = "";
    }
    event.target.className = "active";
    
    if(document.body.offsetWidth < 600){
      let cbox = document.getElementById('content-box');
      event.target.parentElement.insertBefore(cbox, event.target.nextElementSibling); 
    }
  }
  
  contentStripHTML(content:string) {
    var tmp = document.createElement("DIV");
    tmp.innerHTML = content;
    return tmp.textContent || tmp.innerText || "";
  }
}

