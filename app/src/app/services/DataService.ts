import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { People } from '../model/People';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class DataService {
  private jsonDataURL = 'assets/data.json';

  constructor (private http: Http) {}

  getPeople(): Observable<People[]> {
         // ...using get request
         return this.http.get(this.jsonDataURL)
                         .map((res: Response) => res.json())
                         .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
     }
}