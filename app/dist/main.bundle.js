webpackJsonp([1,4],{

/***/ 137:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(44)();
// imports


// module
exports.push([module.i, ".holder {\n\tmargin : 0 auto;\n\twidth:700px;\n}\n\n.header {\n\ttext-align: center;\n}\n\n.nav {\n\twidth: 30%;\n\theight: 100%;\n\tmin-height: 500px;\n\tfloat:left;\n\tborder-right: 1px solid grey;\n}\n\n.nav div:first-child {\n\tmargin-top:40px;\n}\n\n.content {\n\twidth: 65%;\n\tfloat:right;\n}\n\n.content .imgholder {\n\tborder: 5px solid white;\n\tbox-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);\n\twidth: 120px;\n\theight: 120px;\n\tfloat: left;\n    overflow: hidden;\n    margin: 0 10px 10px 0;\n}\n\n.nav div {\n\theight:30px;\n\tborder : 1px solid grey;\n\tbackground-color: rgb(191, 191, 191);\n\ttext-align: center;\n\tborder-right: 0px;\n\tborder-bottom: 0px;\n\tline-height: 27px;\n}\n\n.nav div:last-child {\n\tborder-bottom: 1px solid grey;\n}\n\n.nav div.active {\n\tbackground: white;\n    font-weight: 900;\n}\n\n.nav div span {\n\tmargin-top: 5px;\n\tdisplay:block;\n}\n\n.content img {\n\twidth:100%;\n}\n\n.content h1 {\n\tmargin-top:0px;\n}\n\n\n@media screen and (max-width: 50em) { \n\t.holder {\n\t\twidth:100%;\n\t\tmargin:none;\n\t}\n\t\n\t.nav {\n\t\twidth: 100%;\n\t\tborder-right:none;\n\t\tmin-height: 10px;\n\t}\n\t\n\t.nav div {\n\t\tborder-right: 1px solid grey;\n\t\ttext-align: left;\n\t\tpadding-left: 10px; \n\t\twidth: 100%;\n\t\tfloat: left;\n\t}\n\t\n\t.nav div:first-child {\n\t\tmargin-top:10px;\n\t}\n\t\n\t.nav div.active {\n\t\tborder:none;\n\t}\n\t\n\t.content {\n\t\twidth: 100%;\n\t}\n\t\n\t.nav div.content {\n\t\tfloat: left;\n    \theight: 100%;\n    \tbackground:white;\n    \tborder:none;\n   \t}\n\t\n\t.content h1 {\n\t\tdisplay:none;\n\t}\n}\n\n\n\n\n\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 139:
/***/ (function(module, exports) {

module.exports = "<div class=\"holder\">\n\t<h1 class=\"header\">\n\t  {{title}}\n\t</h1>\n\n\t<div class=\"main\"> \n\t\t<div class=\"nav\">\n\t\t    <div *ngFor=\"let person of savedPeople\" (click)=\"updateProfileContent(person, $event)\">{{person.firstName}} {{person.lastName}}</div>\n\t\t</div>\n\t\t\n\t\t<div id=\"content-box\" class=\"content\">\n\t\t\t<div *ngIf=\"currentImg\" class=\"imgholder\">\n\t\t\t\t<img src=\"assets/{{currentImg}}\">\n\t\t\t</div>\n\t\t\t<h1 *ngIf=\"currentFirstName\">{{currentFirstName}} {{currentLastName}}</h1>\n\t\t\t<ng-container *ngIf=\"currentProfileContent\">\n\t\t\t\t<p *ngFor=\"let content of currentProfileContent\">{{content}}</p>\n\t\t\t</ng-container>\t\n\t\t</div>\n\t</div>\n</div>"

/***/ }),

/***/ 164:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(75);


/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_catch__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DataService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DataService = (function () {
    function DataService(http) {
        this.http = http;
        this.jsonDataURL = 'assets/data.json';
        this.staticData = [{ id: "1", firstName: "Megan", lastName: "Fox", picture: "images/01.jpg", bio: "Megan Denise Fox was born May 16, 1986 in Rockwood, Tennessee. She has one older sister. Megan began her training in drama and dance at the age of 5 and, at the age of 10, moved to Florida where she continued her training and finished school. She now lives in Los Angeles, California. Megan began acting and modeling at the age of 13 after winning several awards at the 1999 American Modeling and Talent Convention in Hilton Head, South Carolina. Megan made her film debut as Brianna Wallace in the Mary-Kate Olsen and Ashley Olsen film, Holiday in the Sun (2001) (V). Her best known role is as Sam Witwicky's love interest Mikaela Banes in the first two installments of the Transformers series: Transformers (2007) and Transformers: Revenge of the Fallen (2009)." }, { id: "2", firstName: "Robert", lastName: "Pattinson", picture: "images/02.jpg", bio: "Robert Pattinson was born on May 13, 1986, in London, England. He enjoys music and is an excellent musician, playing both the guitar and piano.\n\nWhen Robert was 15, he started acting in amateur plays with the Barnes Theatre Company. Afterward, he took screen role like Curse of the Ring (2004) (TV) (Kingdom of Twilight) as Giselher.\n\nIn 2003, Robert took on the role of Cedric Diggory in Harry Potter and the Goblet of Fire (2005). He got his role a week later after meeting Mike Newell in late 2003.\n\nHe has since been cast as Edward Cullen in the highly-anticipated film, Twilight (2008/I). His music will also be heard in the film. Additionally, Robert has completed upcoming roles as Salvador Dali in Little Ashes (2008) and Art in How to Be (2008)." }, { id: "3", firstName: "Jude", lastName: "Law", picture: "images/03.jpg", bio: "Born on December 29, 1972 in southeast London, England, Jude Law is a talented and versatile actor. Law has been nominated for two Academy Awards and continues to build a prolific body of work that spans from early successes such as Gattaca (1997) and The Talented Mr. Ripley (1999) to more recent turns as Dr. John Watson in Sherlock Holmes (2009) and Sherlock Holmes: A Game of Shadows (2011), as Hugo's father in Hugo (2011) and in the titular role in Dom Hemingway (2013)." }, { id: "4", firstName: "Robert", lastName: "Redford", picture: "images/04.jpg", bio: "Born on August 18, 1936, in Santa Monica, California, to Charles Robert Redford, an accountant for Standard Oil, and Martha Hart. His mother died in 1955, the year after he graduated from high school. Charles Robert Redford Jr. was a scrappy kid who stole hubcaps in high school and lost his college baseball scholarship at the University of Colorado because of drunkenness. After studying at the Pratt Institute of Art and living the painter's life in Europe, he studied acting in New York at the American Academy of Dramatic Arts. Lola Van Wagenen (consumer activist), born in 1940, dropped out of college to marry Redford on September 12, 1958. They divorced in 1985 after having four children, one of whom died of sudden infant death syndrome (SIDS). Daughter Shauna Redford, born November 15, 1960, is a painter who married Eric Schlosser on October 5, 1985, in Provo, UT. Her first child, born in January 1991, made Redford a grandfather. Son James Redford (aka Jamie Redford), a screenwriter, was born May 5, 1962. Daughter Amy Redford, an actress; was born October 22, 1970. Redford also has a brother named William.\n\nTelevision and stage experience coupled with all-American good looks led to movies and television roles. His breakthrough role was \"The Sundance Kid\" in Butch Cassidy and the Sundance Kid (1969), when the actor was 32. The Way We Were (1973) and The Sting (1973), both in 1973, made Redford the number one box office star for the next three years. Redford used his clout to advance environmental causes and his riches to acquire Utah property, which he transformed into a ranch and the Sundance ski resort. In 1980, he established the Sundance Institute for aspiring filmmakers. Its annual film festival has become one of the world's most influential. Redford's directorial debut, Ordinary People (1980), won him the Academy Award as Best Director in 1981. He waited eight years before getting behind the camera again, this time for the screen version of John Nichols' acclaimed novel of the Southwest, The Milagro Beanfield War (1988). He scored with critics and fans in 1992 with the Brad Pitt film A River Runs Through It (1992), and again, in 1994, with Quiz Show (1994), which earned him yet another Best Director nomination." }, { id: "5", firstName: "Angelina", lastName: "Jolie", picture: "images/05.jpg", bio: "Stunning, Smart And Slightly Insane, This Hollywood Icon Is Known For Romancing Her Costars, Kissing Her Brother At The Oscars, Exotic Tattoos And Commendable Humanitarian Efforts. The Daughter Of Oscar-Winning Actor Jon Voight, She First Garnered Attention As A High-School Computer Wiz In The Film Hackers After Which She Married Costar Jonny Lee Miller. Their Marriage Died In 1999 Just When Jolie's Career Was Heating Up. She Appeared In Three Films That Year Including The Seriocomic Pushing Tin Which Introduced Her To Hubby No. 2, Billy Bob Thornton, And The Mental-Institution Drama Girl, Interrupted, Which Earned Her An Oscar.\n\nSubsequently, Jolie Made A Series Of Blockbusters (Lara Croft: Tomb Raider) And Bombs (Sky Captain And The World Of Tomorrow), None Of Which Made Use Of Her Considerable Acting Talents. After Divorcing Thornton In 2003, Jolie Devoted Much Of Her Time To Working With Refugees In Impoverished Countries And Raising Her Adopted Children. In 2005, Around The Time Her Action Film Mr. & Mrs. Smith Was Released, Rumors Circulated That She And Recently Separated Costar Brad Pitt Were Having An Affair. Although They Refused To Confirm The Relationship, They Remained Tabloid Cover Stars For Months Until Jolie Finally Admitted In Early 2006 That She Was Pregnant With His Child." }, { id: "6", firstName: "Leondardo", lastName: "DiCaprio", picture: "images/06.jpg", bio: "Leonardo DiCaprio is an American-born actor whose portrayal of doomed suitor Jack Dawson in Titanic (1997) made him a generation's definition of a heartthrob. Throughout his career, DiCaprio has demonstrated a high level of dramatic versatility, from his breakout film role as a mentally-challenged teenager in What's Eating Gilbert Grape (1993), through his work with Martin Scorsese in Gangs of New York (2002) and The Departed (2006). More recently, DiCaprio earned critical notice for his starring roles in Christopher Nolan's Inception (2010) and J. Edgar (2011), for which he received a Golden Globe nomination." }, { id: "7", firstName: "Nicole", lastName: "Kidman", picture: "images/07.jpg", bio: 'Tall, Slender <script>console.log("some script")</script> And Ethereally Beautiful, This Stunning Actress (Who Was Born In The U.S. But Raised Down Under) Began Performing As A Teen And Was Already Well Known In Australia When She Came To Hollywood To Costar With Tom Cruise In The 1990 Racing Flick Days Of Thunder. Within Months The Couple Was Married And The 23-Year-Old Became An Overnight Sensation i Albeit More Due To Her Status As Mrs. Cruise Than Her Talent. Undaunted And Ambitious, Kidman Took On A Variety Of Roles In All Genres Of Films, Although Most Were Unsuccessful: Thrillers (Malice), Tearjerkers (My Life), Historical Epics (Far And Away, Opposite Her Husband) And One Blockbuster (Batman Forever). But It Was Her Seductive Turn As A Wickedly Aspiring Newscaster In The 1995 Black Comedy To Die For That Proved She Was More Than A Pretty Face And Perfect Body. Kidman Worked Steadily For The Rest Of The Decade And Collaborated Once More With Cruise In Eyes Wide Shut. But In 2001 Their 11-Year Marriage Very Publicly Fell Apart As Her Career Heated Up, With An Oscar-Nominated Role In The Musical Extravaganza Moulin Rouge And A Critically Lauded Performance As A Ghost-Plagued Mother In The Others.' }, { id: "8", firstName: "Selena", lastName: "Gomez", picture: "images/08.jpg", bio: 'Selena Marie Gomez (born July 22, 1992) is an American actress, singer, and fashion designer. She is known for portraying Alex Russo, the protagonist in the Emmy Award-winning television series Wizards of Waverly Place. She subsequently ventured into feature films and has starred in the television movies Another Cinderella Story, Wizards of Waverly Place: The Movie, and Princess Protection Program. She made her starring theatrical film debut in Ramona and Beezus.\n\nHer career has expanded into the music industry; Gomez is the lead singer and founder of the pop band Selena Gomez & the Scene, which has released three RIAA Gold certified studio albums, Kiss & Tell, A Year Without Rain, and When the Sun Goes Down, spawned three RIAA Platinum certified singles, "Naturally", "Who Says" and "Love You Like a Love Song" and charted four No. 1 Billboard Hot Dance Club Songs. Gomez has also contributed to the soundtracks of Tinker Bell, Another Cinderella Story, Wizards of Waverly Place, and Shake It Up after signing a record deal with Hollywood Records.' }, { id: "9", firstName: "Beyonce", lastName: "Knowles", picture: "images/09.jpg", bio: "Apart from her work in music, Knowles has also ventured into acting, designing clothes and endorsing various perfumes. Her performance in Dreamgirls eventually earned her two Golden Globe nominations. Knowles introduced her family's fashion line House of Derion in 2005, and has endorsed such brands as L'Orial, Pepsi, Tommy Hilfiger, Nintendo and Vizio. In April 2008, Knowles married rapper Jay-Z. She gave birth to their first child, Blue Ivy Carter, in January 2012." }, { id: "10", firstName: "Jennifer", lastName: "Aniston", picture: "images/10.jpg", bio: "Jennifer Joanna Aniston (born February 11, 1969) is an American actress, film director, and producer. Aniston gained worldwide recognition in the 1990s for portraying Rachel Green on the television sitcom Friends, a role which earned her an Emmy Award, a Golden Globe Award, and a Screen Actors Guild Award. Aniston has also enjoyed a successful Hollywood film career. She gained critical acclaim for her performances in the independent films She's the One (1996), Office Space (1999), The Good Girl (2002) and Friends with Money (2006). She has had her greatest commercial successes with the films Bruce Almighty (2003), The Break-Up (2006), Marley & Me (2008), Just Go with It (2011) and Horrible Bosses (2011). Aniston received a star on the Hollywood Walk of Fame on February 22, 2012." }];
    }
    //  getPeople(): Observable<People[]> {
    //         // ...using get request
    //         return this.http.get(this.jsonDataURL)
    //                         .map((res: Response) => res.json())
    //                         .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    //  }
    DataService.prototype.getPeople = function () {
        return this.staticData;
    };
    return DataService;
}());
DataService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["c" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object])
], DataService);

var _a;
//# sourceMappingURL=DataService.js.map

/***/ }),

/***/ 74:
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = 74;


/***/ }),

/***/ 75:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__(83);




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* enableProdMode */])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 81:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_catch__ = __webpack_require__(64);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(65);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_DataService__ = __webpack_require__(51);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AppComponent = (function () {
    function AppComponent(dataService) {
        this.dataService = dataService;
        this.title = 'Profile Browser';
    }
    AppComponent.prototype.ngOnInit = function () {
        this.getAllPeople();
    };
    AppComponent.prototype.getAllPeople = function () {
        //    this.dataService.getPeople()
        //                       .subscribe(
        //                            data => { this.savedPeople = this.sortPeople(data); },
        //                            err => {
        //                                // Log errors if any
        //                                console.log(err);
        //                          });
        this.savedPeople = this.sortPeople(this.dataService.getPeople());
    };
    AppComponent.prototype.sortPeople = function (data) {
        data.sort(function (a, b) {
            if (a.firstName < b.firstName)
                return -1;
            else if (a.firstName > b.firstName)
                return 1;
            else
                return 0;
        });
        return data;
    };
    AppComponent.prototype.updateProfileContent = function (person, event) {
        this.currentProfileContent = this.contentStripHTML(person.bio);
        this.currentImg = person.picture;
        this.currentFirstName = person.firstName;
        this.currentLastName = person.lastName;
        if (this.currentProfileContent) {
            this.currentProfileContent = this.currentProfileContent.replace(/\n/g, "|NL|").split("|NL|");
        }
        var l = event.target.parentElement.getElementsByClassName('active');
        var count = l.length;
        for (var i = 0; i < count; i++) {
            l[i].className = "";
        }
        event.target.className = "active";
        if (document.body.offsetWidth < 600) {
            var cbox = document.getElementById('content-box');
            event.target.parentElement.insertBefore(cbox, event.target.nextElementSibling);
        }
    };
    AppComponent.prototype.contentStripHTML = function (content) {
        var tmp = document.createElement("DIV");
        tmp.innerHTML = content;
        return tmp.textContent || tmp.innerText || "";
    };
    return AppComponent;
}());
AppComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* Component */])({
        selector: 'app-root',
        template: __webpack_require__(139),
        styles: [__webpack_require__(137)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__services_DataService__["a" /* DataService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__services_DataService__["a" /* DataService */]) === "function" && _a || Object])
], AppComponent);

var _a;
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 82:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_DataService__ = __webpack_require__(51);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["b" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* HttpModule */]
        ],
        providers: [__WEBPACK_IMPORTED_MODULE_5__services_DataService__["a" /* DataService */]],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 83:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ })

},[164]);
//# sourceMappingURL=main.bundle.js.map